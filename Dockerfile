FROM ghcr.io/graalvm/graalvm-ce
RUN microdnf upgrade -y && microdnf install -y rsync git openssh-server
RUN gu install js

# build scriptcraft
env ANT_VERSION=1.10.12
ADD https://dlcdn.apache.org/ant/binaries/apache-ant-${ANT_VERSION}-bin.tar.gz /tmp/apache-ant-${ANT_VERSION}-bin.tar.gz
RUN cd /opt/ && tar -xzvf /tmp/apache-ant-${ANT_VERSION}-bin.tar.gz
ENV ANT_HOME /opt/apache-ant-${ANT_VERSION}
ENV PATH ${PATH}:${ANT_HOME}/bin
RUN ls ${ANT_HOME}
RUN git clone https://github.com/ediloren/ScriptCraft.git /opt/ScriptCraft \
 && cd /opt/ScriptCraft \
 && ant package \
 && mkdir -p /opt/minecraft/plugins/ \
 && cp /opt/ScriptCraft/target/`date "+%Y-%m-%d"`/scriptcraft.jar /opt/minecraft/plugins/scriptcraft.jar

# Spigot (Minecraft server)
add https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar /opt/minecraft/BuildTools.jar
workdir /opt/minecraft/

env MINECRAFT_VERSION=1.19
run java -jar BuildTools.jar --rev $MINECRAFT_VERSION --compile craftbukkit

run echo "eula=true" > /opt/minecraft/eula.txt
add server.properties /opt/minecraft/server.properties
#add config.yml /opt/minecraft/plugins/scriptcraft/config.yml

# a default ssh access to upload js
RUN ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa \
 && ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa \
 && ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa \
 && ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
add sshd_config /etc/ssh/sshd_config
run mkdir -p /opt/minecraft/scriptcraft/players/
run echo "root:minecraft" | chpasswd

add start /start
run chmod +x /start

expose 25565 22
volume ["/minecraft/"]
cmd /start
